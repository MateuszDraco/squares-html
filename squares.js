/*jslint browser: true*/
/*global $, jQuery, alert*/

var map0 = '{"size": {"w": 5, "h": 1}, "start": {"x": 1, "y": 1}, "end": {"x": 5, "y": 1}, "blocks": []}';
var res0 = '{"name": "Victoria", "steps": ["right", "right", "right", "right"]}';
var map1 = '{"size": {"w": 5, "h": 4}, "start": {"x": 1, "y": 1}, "end": {"x": 5, "y": 4}, "blocks": [{"x": 2, "y": "2"}, {"x": 3, "y": 3}]}';
var map2 = '{"size": {"w": 15, "h": 8}, "start": {"x": 1, "y": 1}, "end": {"x": 5, "y": 4}, "blocks": [{"x": 2, "y": "2"}, {"x": 3, "y": 3}, {"x":11, "y":4}, {"x":4, "y":1}]}';

//{"name": "Victoria", "steps": ["left", "up", "buu", "right", "right", "left", "right", "up", "right"]}

function emptyRow() {
  return $('<div class="sq-row"></div>');
}

function emptyCell() {
  return $('<div class="sq"></div>');
}

function createHeader(cols) {
  var row = emptyRow();
  emptyCell().addClass('none').appendTo(row);
  for (var r = 0; r < cols; ++r) {
    emptyCell()
      .addClass('head')
      .text(r + 1)
      .appendTo(row);
  }
  emptyCell().addClass('none').appendTo(row);
  return row;
}

function createRow(r, cols) {
  var row = emptyRow();
  emptyCell().addClass('head').text(r).appendTo(row);
  for (var i = 0; i < cols; ++i) {
    emptyCell()
      .attr('id', getCellId(i + 1, r))
      .attr('data-x', i + 1)
      .attr('data-y', r)
      .addClass('cell')
      .appendTo(row);
  }
  emptyCell().addClass('head').text(r).appendTo(row);
  return row;
}

function getCellId(x, y) {
  return 'x'+ x + 'y' + y;
}

function getCell(x, y) {
  return $('#map #' + getCellId(x, y))[0];
}

function createMap(cols, rows) {
  var map = $("#map").html('');
  createHeader(cols).appendTo(map);
  for (var row = rows; row > 0; --row) {
    createRow(row, cols).appendTo(map);
  }
  createHeader(cols).appendTo(map);
}

function markCell(cellPos, cellClass) {
  if (cellPos != null && cellPos.x != null && cellPos.y != null) {
    var cell = getCell(cellPos.x, cellPos.y);
    if (cell != null) {
      $(cell).addClass(cellClass);
    }
  }
}

function markStart(start) {
  markCell(start, 'start');
}

function markEnd(end) {
  markCell(end, 'end');
}

function markBlocks(blocks) {
  if (blocks != null) {
    for (var i = 0; i < blocks.length; i++) {
      markCell(blocks[i], 'block');
    }
  }
}

function prepareMap(json) {
  if (json != null) {
    var jmap = $.parseJSON(json);
    createMap(jmap.size.w, jmap.size.h);
    markStart(jmap.start);
    markEnd(jmap.end);
    markBlocks(jmap.blocks);
  }
}

function walkStep(x, y, step, steps) {
  var nx = x;
  var ny = y;
  switch (steps[step].toLowerCase()) {
    case "up": ny++; break;
    case "down": ny--; break;
    case "left": nx--; break;
    case "right": nx++; break;
    default:
  }
  var cell = $(getCell(nx, ny) || getCell(x, y));
  cell.text(step + 1);
  step++;

  if (step < steps.length) {
    x = cell.attr('data-x');
    y = cell.attr('data-y');

    setTimeout(function () {
      walkStep(x, y, step, steps);
    }, 500);
  }
}

function walk(steps) {
  if (steps != null) {
    var start = $("#map .sq.start");
    var x = start.attr('data-x');
    var y = start.attr('data-y');

    walkStep(x, y, 0, steps);
  }
}

function parseResult(json) {
  $('#map .sq.cell').text('');
  if (json != null) {
    var jresult = $.parseJSON(json);
    walk(jresult.steps);
  }
}

$(function () {
  var map = map0;
  prepareMap(map);
  $('#schema-gen textarea').val(map);

  $('#schema-gen button').click(function (event) {
    prepareMap($('#schema-gen textarea').val());
  });

  $('#walk textarea').val(res0);
  $('#walk textarea').val('{"name": "Victoria", "steps": ["left", "up", "buu", "right", "right", "left", "right", "up", "right"]}');
  $('#walk button').click(function (event) {
    parseResult($('#walk textarea').val());
  });
});
